export ISE_EIFFEL=/opt/Eiffel71
export ISE_PLATFORM=linux-x86-64
EIFGENS=/var/tmp/EIFGENs
EBIN=$(ISE_EIFFEL)/studio/spec/$(ISE_PLATFORM)/bin/
EC=$(EBIN)/ec
EF=$(EBIN)/finish_freezing

bowlinggametdd: bowlinggametdd.ecf application.e game.e
	$(EC) -batch  -config $< 
	(cd EIFGENs/$@/W_code; $(EF) )
	mv EIFGENs/$@/W_code/$@ .


.PHONY: clean

clean:
	rm -f bowlinggametdd
	rm -rf EIFGENs/bowlinggametdd
	rm -f EIFGENs
	ln -s $(EIFGENS)
