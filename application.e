note
	description : "BowlingGameTDD application root class"
	date        : "$Date$"
	revision    : "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization
	g: Game

	frame(a,b: INTEGER)
		require
			no_strike: a /= 10
			max_ten_pins: a + b <= 10
		do
			print("Roll " + a.out + "/" + b.out + "%N")
			g.roll(a)
			g.roll(b)
			if a + b = 10 then
				print("Spare! ")
			end
			print("Score is "+ g.score.out + "%N")
		end

	strike
		do
			g.roll(10)
			print("Strike! Score is: " + g.score.out + "%N")
		end
	
	make
			-- Run application.
		do
			create g.make
			frame(1,4)
			check
				frame1: g.score = 5
			end
			frame(4,5)
			check
				frame2: g.score = 14
			end
			frame(6,4)
			check
				frame3: g.score = 24
			end
			frame(5,5)
			check
				frame4: g.score = 39
			end
			strike
			check
				frame5: g.score = 59
			end
			frame(0,1)
			check
				frame6: g.score = 61
			end
			frame(7,3)
			check
				frame7: g.score = 71
			end
			frame(6,4)
			check
				frame8: g.score = 87
			end
			strike
			check
				frame9: g.score = 107
			end
			frame(2,8)
			check
				frame10: g.score = 127
			end
			g.roll(6)
			check
				frame11: g.score = 133
			end
			print("Score is " + g.score.out + "%N")
		end

end
