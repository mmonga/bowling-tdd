note
	description: "Summary description for {GAME}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

class
	GAME

create
	make

feature

	roll (pins: INTEGER)
		require
			0 <= pins and pins <= 10
		do
			rolls [cur_roll] := pins
			cur_roll := cur_roll + 1
		ensure
			score >= old score + pins
		end

	score: INTEGER
		local
			frame_idx: INTEGER
			i: INTEGER
		do
			from
				frame_idx := rolls.lower
				i := 0
			until
				i = 10
			loop
				if is_strike (frame_idx) then
					Result := Result + 10 + strike_bonus (frame_idx)
					frame_idx := frame_idx + 1
				elseif is_spare (frame_idx) then
					Result := Result + 10 + spare_bonus (frame_idx)
					frame_idx := frame_idx + 2
				else
					Result := Result + frame_value (frame_idx)
					frame_idx := frame_idx + 2
				end
				i := i + 1
			end
		end

feature {NONE} -- Initialization

	rolls: ARRAY [INTEGER]

	cur_roll: INTEGER

	Max_rolls: INTEGER = 21

	make
			-- Initialization for `Current'.
		do
			create rolls.make_filled (0, 1, Max_rolls)
			cur_roll := rolls.lower
		end

	is_spare (frame: INTEGER): BOOLEAN
		require
			1 <= frame and frame <= Max_rolls - 1
		do
			Result := rolls [frame] + rolls [frame + 1] = 10
		end

	is_strike (frame: INTEGER): BOOLEAN
		require
			1 <= frame and frame <= Max_rolls
		do
			Result := rolls [frame] = 10
		end

	strike_bonus (frame: INTEGER): INTEGER
		require
			1 <= frame and frame <= Max_rolls - 2
		do
			Result := rolls [frame + 1] + rolls [frame + 2]
		end

	spare_bonus (frame: INTEGER): INTEGER
		require
			1 <= frame and frame <= Max_rolls - 2
		do
			Result := rolls [frame + 2]
		end

	frame_value (frame: INTEGER): INTEGER
		require
			1 <= frame and frame <= Max_rolls - 1
		do
			Result := rolls [frame] + rolls [frame + 1]
		end

	sum_pins (a: ARRAY [INTEGER]): INTEGER
		do
			across
				a as i
			loop
				Result := Result + i.item
			end
		end

invariant
	0 <= score and score <= 300
	1 <= cur_roll and cur_roll <= Max_rolls

end
