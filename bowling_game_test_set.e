note
	description: "[
		Eiffel tests that can be executed by testing tool.
	]"
	author: "EiffelStudio test wizard"
	date: "$Date$"
	revision: "$Revision$"
	testing: "type/manual"

class
	BOWLING_GAME_TEST_SET

inherit

	EQA_TEST_SET
		redefine
			on_prepare
		end

feature -- Test routines

	test_gutter_game
		do
			roll_many (20, 0)
			assert ("Gutter game is 0: " + g.score.out, 0 = g.score)
		end

	test_all_ones
		do
			roll_many (20, 1)
			assert ("All ones game is 20: " + g.score.out, 20 = g.score)
		end

	test_one_spare
		do
			roll_spare
			g.roll (3)
			roll_many (17, 0)
			assert ("One initial spare gives 16: " + g.score.out, 10 + 3 * 2 = g.score)
		end

	test_one_strike
		do
			roll_strike
			g.roll (3)
			g.roll (4)
			roll_many (16, 0)
			assert ("One initial strike gives 24: " + g.score.out, 10 + 3 * 2 + 4 * 2 = g.score)
		end

	test_perfect_game
		do
			roll_many (12, 10)
			assert ("A perfect game is 300: " + g.score.out, 300 = g.score)
		end

feature {NONE}

	g: GAME

	on_prepare
		do
			create g.make
		end

	roll_many (n, pins: INTEGER)
		do
			across
				1 |..| n as i
			loop
				g.roll (pins)
			end
		end

	roll_spare
		do
			g.roll (5)
			g.roll (5)
		end

	roll_strike
		do
			g.roll (10)
		end

end
